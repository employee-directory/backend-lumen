<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // $this->call('UsersTableSeeder');

        // php artisan db:seed to run
        App\Models\User::truncate();
        App\Models\Employee::truncate();
        App\Models\Department::truncate();

        $user = new App\Models\User();
        $user->email = 'example@gmail.com';
        $user->name = 'Administrator';
        $user->password = Hash::make('123456');
        $user->save();
        // Using faker
        $faker = Faker::create();
        foreach (range(0, 300) as $index) {
            foreach (range(0, 100) as $count) {
                DB::table('employees')->insert([
                    'name' => $faker->name,
                    'title' => $faker->title,
                    'job_title' => $faker->jobTitle,
                    'department_id' => $index + 1,
                    'email' => $faker->email,
                    'phone_number' => $faker->phoneNumber,
                    'image' => 'uploads/images/icon-user-default.png'
                ]);
            }
        }
        foreach (range(0, 300) as $index) {
            DB::table('departments')->insert([
                'name' => $faker->company,
                'office_number' => $faker->tollFreePhoneNumber,
                'manager_id' => $index + 1
            ]);
        }
    }
}
