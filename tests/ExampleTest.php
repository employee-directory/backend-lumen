<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ExampleTest extends TestCase {

    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testApplication() {
        $user = factory('App\Models\User')->create();

        $this->actingAs($user)
            ->get('/user');

        $this->seeInDatabase('users', ['email' => $user->email]);

    }
}
