<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class EmployeeTest extends TestCase {
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testEmployee() {
        $user = \App\Models\User::find(1);
        //create Department
        $employee = $this->actingAs($user, 'api')->call('POST', '/employees/store', [
            'name' => 'magid',
            'email' => 'magid@gmail.com',
            'phone_number' => '123456',
            'title' => 'Mr.',
            'job_title' => 'Software',
            'department_id' => 1
        ]);
        $this->assertEquals(200, $employee->status());
        $employee = json_decode($employee->content(), true);


        //Update Department
        $employeeUpdate = $this->actingAs($user, 'api')->call('POST', '/departments/update', [
            'employee_id' => $employee['id'],
            'name' => 'magid 1',
            'email' => 'magid@gmail.comx',
            'phone_number' => '123456',
            'title' => 'Mr.',
            'job_title' => 'Software',
            'department_id' => 1
        ]);
        $this->assertEquals(200, $employeeUpdate->status());
    }
}
