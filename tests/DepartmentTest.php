<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class DepartmentTest extends TestCase {
    use DatabaseTransactions;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testDepartment() {
        $user = \App\Models\User::find(1);
        //create Department
        $department = $this->actingAs($user, 'api')->call('POST', '/departments/store', [
            'name' => 'test department',
            'description' => 'test',
            'office_number' => '123123',
            'manager_id' => 1
        ]);
        $this->assertEquals(200, $department->status());
        $department = json_decode($department->content(), true);


        //Update Department
        $departmentUpdate = $this->actingAs($user, 'api')->call('POST', '/departments/update', [
            'department_id' => $department['id'],
            'name' => 'test department 1',
            'description' => 'test 1',
            'office_number' => '123123 1',
            'manager_id' => 1
        ]);
        $this->assertEquals(200, $departmentUpdate->status());
    }
}
