<?php

namespace App\Http\Controllers\Departments;

use App\Helpers\Paging;
use App\Http\Controllers\Controller;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * @return Paging
     */
    public function list() {
        $departments = Department::withCount('employees')->with('manager')->orderBy('id', 'DESC');

        return new Paging($departments);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function one($id) {
        $department = Department::find($id);

        return $department;
    }

    /**
     * @param Request $request
     * @return Department
     */
    public function store(Request $request) {
        $inputs = $request->all();
        $validator = Validator::make($inputs, [
            'name' => 'required',
            'office_number' => 'required',
            'manager_id' => 'required|exists:employees,id'
        ]);

        if ($validator->fails()) {
            return ['status' => 'failed', 'items' => $validator->errors()];
        }
        $department = new Department();
        $department->fill($inputs);
        $department->save();

        return $department;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request) {
        $inputs = $request->all();
        $validator = Validator::make($inputs, [
            'department_id' => 'required|exists:departments,id',
            'name' => 'required',
            'office_number' => 'required',
            'manager_id' => 'required|exists:employees,id'
        ]);

        if ($validator->fails()) {
            return ['status' => 'failed', 'items' => $validator->errors()];
        }
        $department = Department::find($inputs['department_id']);
        $department->fill($inputs);
        $department->save();

        return $department;
    }
}
