<?php

namespace App\Http\Controllers\Employees;

use App\Helpers\Paging;
use App\Http\Controllers\Controller;
use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EmployeeController extends Controller {
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        //
    }

    /**
     * @return Paging
     */
    public function list(Request $request) {
        $filter = $request->all();
        $employees = Employee::with('department.manager');

        if (empty($filter['managers'])) {
            $employees = $employees->orderByDesc('id');
        }
        if (!empty($filter['job_title'])) {
            $employees = $employees->where('job_title', $filter['job_title']);
        }

        if (!empty($filter['department_id'])) {
            $employees = $employees->where('department_id', $filter['department_id']);
        }

        if (!empty($filter['term'])) {
            $employees = $employees->where('name', 'like', '%' . $filter['term'] . '%');
        }
        return new Paging($employees);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function one($id) {
        $employee = Employee::where('id', $id)->with('department.manager')->first();

        return $employee;
    }

    /**
     * @param Request $request
     * @return Employee
     */
    public function store(Request $request) {
        $inputs = $request->all();

        $validator = Validator::make($inputs, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'job_title' => 'required',
            'department_id' => 'required|exists:departments,id'
        ]);

        if ($validator->fails()) {
            return ['status' => 'failed', 'items' => $validator->errors()];
        }

        $employee = new Employee();
        $employee->fill($inputs);
        $employee->save();

        //Save Image
        if ($request->hasFile('image')) {
            $employee->image = $this->uploadImage($request, $employee);
            $employee->save();
        }

        return $employee;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function update(Request $request) {
        $inputs = $request->all();
        $validator = Validator::make($inputs, [
            'employee_id' => 'required|exists:employees,id',
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'job_title' => 'required',
            'department_id' => 'required|exists:departments,id'
        ]);

        if ($validator->fails()) {
            return ['status' => 'failed', 'items' => $validator->errors()];
        }

        $employee = Employee::find($inputs['employee_id']);
        $employee->fill($inputs);
        $employee->save();

        //Save Image
        if ($request->hasFile('image')) {
            $employee->image = $this->uploadImage($request, $employee);
            $employee->save();
        }

        return $employee;
    }

    /**
     * @param $request
     * @return mixed
     */
    protected function uploadImage($request, $employee) {
        if ($request->file('image') != null) {
            $image = $request->file('image');
            $imageUrl = time() . '_' . $employee->id . '.' . $image->getClientOriginalExtension();
            $destinationPath = public_path("uploads/images");

            $image->move($destinationPath, $imageUrl);
            return 'uploads/images/' . $imageUrl;
        }
        return "";
    }

    public function getJobTitles() {
        $titles = Employee::groupBy('job_title')->pluck('job_title');

        return $titles;
    }
}
