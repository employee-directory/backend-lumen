<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Support\Str;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class User extends Model implements AuthenticatableContract, AuthorizableContract {
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    public function getJWTIdentifier() {
        return $this->getKey();
    }

    public function getJWTCustomClaims() {

        //create jwt sign on the fly
        if (!$this->jwt_sign) {
            $this->updateJWTSign();
        }

        $claims = ["jwt_sign" => $this->jwt_sign];

        return $claims;
    }

    /**
     * Functions
     */
    public function updateJWTSign() {
        $this->jwt_sign = Str::random();
        $this->save();
    }

    /**
     * generate Token
     * */
    public function generateJWTToken($ttl = 0) {
        if ($ttl) {
            JWTAuth::factory()->setTTL($ttl);
        }

        return JWTAuth::fromUser($this);
    }
}
