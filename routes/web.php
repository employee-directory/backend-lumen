<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$router->group(['namespace' => 'User', 'prefix' => 'admin/auth'],
    function () use ($router) {
        $router->post('login','UserController@authenticate');
    }
);

$router->group(
    ['namespace' => 'Departments', 'prefix' => 'departments', 'middleware' => 'auth'],
    function () use ($router) {
        $router->get('list', 'DepartmentController@list');
        $router->get('one/{id}', 'DepartmentController@one');
        $router->post('store', 'DepartmentController@store');
        $router->post('update', 'DepartmentController@update');
    });

$router->group(
    ['namespace' => 'Employees', 'prefix' => 'employees', 'middleware' => 'auth'],
    function () use ($router) {
        $router->get('list', 'EmployeeController@list');
        $router->get('one/{id}', 'EmployeeController@one');
        $router->post('store', 'EmployeeController@store');
        $router->post('update', 'EmployeeController@update');
        $router->get('job-titles', 'EmployeeController@getJobTitles');
    });
