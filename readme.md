# Employees Directory

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Simple implementation of employee directory with departments and job titles

## Official Lumen Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Installation
1- git clone git@gitlab.com:employee-directory/backend-lumen.git<br/>
2- copy .env.example to .env<br/>
3- update .env with your local mysql settings<br/>
5- run composer install<br/>
6- run php artisan migrate --seed<br/>

## Serve
1- cd public<br/>
2- run php -S localhost:8000

## Test
1- if you have phpunit installed globally run "phpunit"
else run "vendor/bin/phpunit"
